$(function() {
    $('.bannerCarousel, .booksCarousel').owlCarousel({
        loop: true,
        dots: true,
        margin:0,
        items: 1,
        responsive: {
            0: {
                items: 1
            }
        }
    })
    $('.halfbgowl').owlCarousel({
        loop: true,
        dots: true,
        items: 1
    })
    $(".interestingCarousel, .recommendationCarousel").owlCarousel({
        loop: true,
        dots: true,
        nav: true,
        margin: 20,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            600: {
                items: 2,
                nav: false
            },
            992: {
                items: 3
            }
        }
    })
    $(".bestSellers__carousel, .newitems__carousel, .booksSale__carousel").owlCarousel({
        loop: true,
        dots: true,
        nav: true,
        margin: 50,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            600: {
                items: 2,
                nav: false
            },
            992: {
                items: 3
            },
            1200: {
                items: 5
            }
        }
    })
});